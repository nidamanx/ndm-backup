#!/usr/bin/env bash
#
# NDM Backup
# Save important parts of your GNU/Linux system
#

readonly K_VERSION='1.5.10'

: '
Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.

This code is licensed under GPLv3 license (see LICENSE for details)
The above copyright notice and this permission notice shall be
included and visible in the top of all copies or portions of this
Software.

This program is free software.
You can use, redistribute and/or modify it under the terms of the
GNU General Public License published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Author, Owner, Maintainer and Contributors are not responsable for
any damage or data loss. Use at your own risk.
'

### DESCRIPTION ###
# Save important parts of your GNU/Linux system
### INSTALL ###
# 1. Save this script on your computer (eg: `/usr/local/bin/` `~/.local/bin/` or `~/bin/`)
# 2. Change permission as follows: `chmod +x ndm_backup`
### CONFIGURE ###
# Configure the parameters in the appropriare section
### USAGE ###
# `sudo ndm_backup` or `su -c 'ndm_backup'`
### LICENSE ###
# Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.
# This sowtware is licensed under GPLv3 license.


##################################################################
# START EDITING BELOW THIS LINE                                  #
##################################################################


########################
# CONFIGURE
########################

# Hostname to use as backup name suffix
# Example: v_hostname="$(v_hostname)"
v_hostname="$(hostname)"

# Username used to store the backup
# Example: v_username="$(users)"
v_username="$(users)"

# Backup name
# Example: backup_name='NDM_BK'
v_backup_name='NDM_BK'

# Where to backup
# Example: dest="/home/${v_username}/backup"
v_dest="/home/${v_username}/backup"

# Working directory to save files before tar
# Example: workdir="/tmp/${v_backup_name}"
v_workdir="/tmp/${v_backup_name}"

# List of storages
# Example: a_storage=( "sda" "sdb" )
a_storage=( "sda" "sdb" )

# List of encrypted LUKS partitions
# Example: a_luks=( "sda3" "sdb3" )
a_luks=( "sda3" "sdb3" )

# Change tar archive ownership to current user
# Example: v_tar_owner=true
v_tar_owner=true

# Pre and post command
# Optionally add here your own command (e.g. stop/start services, etc.)
# Example: v_pre_command="echo 'echo 'Begin $backup_name'"
# Example: v_post_command="echo 'echo 'End $backup_name'"
v_pre_command=""
v_post_command=""

# Optionally, rsync between $dest and a locale or remote directory
# Example: v_dest_rsync="/mnt/device/backup"
v_dest_rsync=""

# Optionally, add extra sources to include to the backup
# Example: a_source_extra=( "/root" "/etc" "/usr/local/bin" )
a_source_extra=( "/root" "/etc" "/usr/local/bin" )

# Language for datetime suffix
# Example (no locale): export LC_TIME=C
# Example (locale German): export LC_TIME="de_DE.UTF-8"
# Example (locale UK English): export LC_TIME="en_GB.UTF-8"
# Example (locale Spanish): export LC_TIME="es_ES.UTF-8"
# Example (locale French): export LC_TIME="fr_FR.UTF-8"
# Example (locale Italian): export LC_TIME="it_IT.UTF-8"
export LC_TIME=C

# Backup name datetime suffix
# Example ISO: v_datetime_suffix="$(date +'%Y-%m-%d_%H-%M-%S')"
# Example WeekDay: v_datetime_suffix="$(date '+%A')"
v_datetime_suffix="$(date +'%w-%a')"

# MySQL/MariaDB
# Backup all in one single file
# Example: v_mysql_single_dump=false
v_mysql_single_dump=false
# Get user and pass from .my.cnf file
# Example: v_mysql_cnf_path="${HOME}/.my.cnf"
# Example: v_mysql_cnf_path="$(dirname "${BASH_SOURCE[0]}")/.my.cnf"
# Example: v_mysql_cnf_path="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")/.my.cnf"
# Example: v_mysql_cnf_path="your_custom_path/.my.cnf"
v_mysql_cnf_path="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")/.my.cnf"


##################################################################
# DO NOT EDIT BELOW THIS LINE UNLESS YOU KNOW WHAT YOU ARE DOING #
##################################################################


# Set vars and arrays as readonly to be safe
readonly v_hostname
readonly v_username
readonly v_backup_name
readonly v_dest
readonly v_workdir
readonly a_storage
readonly a_luks
readonly v_tar_owner
readonly v_pre_command
readonly v_post_command
readonly v_dest_rsync
readonly a_source_extra
readonly v_datetime_suffix
readonly v_mysql_single_dump
readonly v_mysql_cnf_path


# For debug purpose
readonly K_DEBUG=false


########################
# FUNCTIONS
########################


# Print usage
function f_usage() {
  local v_usage=\
"
Usage: sudo ndm_backup [-s[-q[-k]]] | [-h|-u|-v]
"
  printf "%s\n" "${v_usage}"
}

# Print help
function f_help() {
  local v_help=\
"
Usage: sudo ndm_backup [OPTION]
NDM Backup saves important parts of your GNU/Linux system

Examples:
  ndm_backup --start
  ndm_backup --start --quiet
  ndm_backup --start --quiet --keep

 Main operation mode:
  -s, --start      start backup

 Operation modifiers:
  -k, --keep       don't delete temporary directory in quiet mode
  -q, --quiet      don't ask questions and don't show any progress

 Other options:
  -h, --help       show this help
  -u, --usage      show a short usage message
  -v, --version    show software version
"
  printf "%s\n" "${v_help}"
}

# Check if all commands are available
function f_check_command() {
  command -v "${1}" >/dev/null 2>&1 || {
    printf "\nCommand '%s' is required but not installed.\n\n" "${1}" >&2
    exit 1
  }
}

# Check confirmations
function f_ask_confirmation() {
  while true; do
    printf "\n%s" "${1}"
    read -r v_choice
    case "${v_choice}" in
      Y|Yes )
        if [[ "${2}" == "${v_workdir}" ]]; then
          printf "Deleting directory %s\n" "${v_workdir}"
          rm -rf "${v_workdir}"
          printf "Done\n"
        else
          printf "OK, preparing...\n\n"
        fi
        break
        ;;
      N|No )
        printf "Nothing is done\n"
        if [[ "${2}" != "${v_workdir}" ]]; then
          printf "You need to move %s in another location\n\n" "${v_workdir}"
          exit 0
        fi
        break
        ;;
      C|Cancel )
        printf "Script aborted\n\n"
        exit 0
        ;;
      * )
        printf "Invalid answer (check uppercase)\n\n" >&2
        ;;
    esac
  done
}

# Show message
function f_print_msg() {
  if [[ "${v_quiet}" == false ]]; then
    case "${#}" in
      1 )
        printf "%s" "${1}"
        ;;
      2 )
        printf "%s\n" "${1}"
        ;;
      3 )
        printf "\n%s\n" "${2}"
        ;;
      * )
        printf ""
        ;;
    esac
  fi
}

# Show msg for done parts
function f_backup_done_msg() {
  if [[ "${v_quiet}" == false ]]; then
    printf "%s\nDone\n" "${1}"
  fi
}

# Show msg for skipped parts
function f_backup_skipped_msg() {
  if [[ "${v_quiet}" == false ]]; then
    printf "%s already exists\nSkipped\n" "${1}"
  fi
}

# Print version
function f_version() {
  local v_author='Nicola Davide Mannarelli - NDMnet'
  local v_name='NDM Backup'
  printf "\n%s version %s\n%s\n\n" "${v_name}" "${K_VERSION}" "${v_author}"
}


########################
### MAIN             ###
########################

function main() {

  ### BASIC CHECKS ###  
  
  # Check SuperUser
  if [[ "${USER}" != 'root' ]]; then
    printf "\nThis script needs SuperUser privileges\n\n" >&2
    exit 1
  fi
  
  # Check needed commands
  readonly a_needed_commands=( "fdisk" "sfdisk" "lshw" "lsblk" "blkid" "inxi" )
  for i in "${a_needed_commands[@]}"; do
    f_check_command "${i}"
  done
  
  # Ask for confirmation to use this script
  if [[ "${v_quiet}" == false ]]; then
    v_msg="Do you really want to execute this script? ([Y]es/[N]o/[C]ancel)? "
    f_ask_confirmation "${v_msg}"
  fi
  
  # Ask for confirmation to use an existing temp directory
  if [[ "${v_quiet}" == false ]]; then
    if [[ -d "${v_workdir}" ]]; then
      printf "Directory '%s' already exists" "${v_workdir}"
      v_msg="Do you really want to use it? ([Y]es/[N]o/[C]ancel)? "
      f_ask_confirmation "${v_msg}"
    fi
  fi
  
  ### BASIC INFOS ###
  
  # Get if UEFI or BIOS setup
  if [[ -d /sys/firmware/efi/ ]]; then
    v_uefi_bios='uefi'
  else
    v_uefi_bios='bios'
  fi
  f_print_msg "Your system is based on: ${v_uefi_bios}" "\n"
  
  # Get partition type (gpt or mbr)
  declare -a a_gpt
  declare -a a_mbr
  for i in "${a_storage[@]}"; do
    declare "v_disklabel_type_${i}=$(fdisk -l /dev/sda | grep Disklabel | awk '{print $3}')"
    v_temp="v_disklabel_type_$i"
    f_print_msg "Your partition type for /dev/${i} is: ${!v_temp}" "\n"
    if [[ "${!v_temp}" == 'mbr' ]]; then
      a_mbr+=("${i}")
      # v_mbr="$(echo "${v_mbr} ${i}" | xargs)"
    elif [[ "${!v_temp}" == 'gpt' ]]; then
      a_gpt+=("${i}")
      # v_gpt="$(echo "${v_gpt}\n${i}" | xargs)"
    fi
  done
  readonly a_gpt
  readonly a_mbr
  
  ### START BACKUP ###
  
  # Execute pre command
  if [[ "${v_pre_command}" ]]; then
    f_print_msg "\n" "Executing pre command" "\n"
    eval "${v_pre_command}"
  fi
  
  # Directories creation
  if [[ ! -d "${v_dest}" ]]; then
    mkdir -p "${v_dest}"
  fi
  if [[ ! -d "${v_workdir}" ]]; then
    mkdir -p "${v_workdir}"
  fi
  if [[ "${v_dest_rsync}" ]] && [[ ! -d "${v_dest_rsync}" ]]; then
    mkdir -p "${v_dest_rsync}"
  fi
  
  # Hardware info
  f_print_msg "\n" "Backing up Hardware list with lshw" "\n"
  v_temp_path="${v_workdir}"/lshw.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    lshw > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # List PCI and USB Devices
  f_print_msg "\n" "Backing up list of PCI and USB devices" "\n"
  v_temp_path="${v_workdir}"/devices
  if [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    lspci -nn > "${v_temp_path}"/lspci.txt
    lsusb > "${v_temp_path}"/lsusb.txt
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # List Block Devices
  f_print_msg "\n" "Backing up lsblk" "\n"
  v_temp_path="${v_workdir}"/lsblk.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    lsblk --all --fs > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # List of all installed devices ID
  f_print_msg "\n" "Backing up installed devices ID list" "\n"
  v_temp_path="${v_workdir}"/blkid.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    blkid > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # HW/SW info
  f_print_msg "\n" "Backing up infos with inxi" "\n"
  v_temp_path="${v_workdir}"/inxi.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    inxi --full > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # UEFI
  f_print_msg "\n" "Backing up EFI" "\n"
  v_temp_path="${v_workdir}"/efi
  if [[ "${v_uefi_bios}" == 'uefi' ]] && [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    efibootmgr --verbose > "${v_temp_path}"/efibootmgr.txt
    cp -a /boot/efi "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "No UEFI or ${v_temp_path}"
  fi
  
  # MBR
  f_print_msg "\n" "Backing up MBR BIOS" "\n"
  v_temp_path="${v_workdir}"/mbr
  if [[ "${#a_mbr[@]}" != 0 ]] && [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    for i in "${a_mbr[@]}"; do
      f_print_msg "Backing up MBR for /dev/${i}" "\n"
      dd if="/dev/$i" of="${v_temp_path}/${i}_mbr.bin" bs=512 count=1 >/dev/null 2>&1
    done
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "No BIOS or ${v_temp_path}"
  fi
  
  # LUKS
  f_print_msg "\n" "Backing up LUKS info" "\n"
  v_temp_path="${v_workdir}"/luks
  if [[ "${#a_luks[@]}" != 0 ]] && [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"/luks
    for i in "${a_luks[@]}"; do
      cryptsetup luksDump "/dev/$i" > "${v_temp_path}/luksDump_${i}.txt"
      cryptsetup luksUUID "/dev/$i" > "${v_temp_path}/luksUUID_${i}.txt"
      cat /etc/crypttab > "${v_temp_path}"/crypttab.txt
    done
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "No LUKS or ${v_temp_path}"
  fi
  
  # LVM
  f_print_msg "\n" "Backing up LVM info" "\n"
  v_temp_path="${v_workdir}"/lvm
  if [[ "$(command -v lvmdump)" ]] && [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    lvmdump -d "${v_temp_path}"/lvmdump >/dev/null 2>&1
    pvdisplay --verbose > "${v_temp_path}"/pvdisplay.txt
    vgdisplay --verbose > "${v_temp_path}"/vgdisplay.txt
    lvdisplay --verbose > "${v_temp_path}"/lvdisplay.txt
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "No LVM or ${v_temp_path}"
  fi
  
  # OS Release
  f_print_msg "\n" "Backing up OS release" "\n"
  v_temp_path="${v_workdir}"/lsb_release.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    lsb_release --all 2>/dev/null | grep -v "No LSB modules" > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # Kernel Release
  f_print_msg "\n" "Backing up Kernel release" "\n"
  v_temp_path="${v_workdir}"/uname.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    uname --all > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # Partitions
  f_print_msg "\n" "Backing up Partition Table" "\n"
  v_temp_path="${v_workdir}"/partitions
  if [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    fdisk -l /dev/sda > "${v_temp_path}"/fdisk_sda.txt
    fdisk -l /dev/sdb > "${v_temp_path}"/fdisk_sdb.txt
    sfdisk -d /dev/sda > "${v_temp_path}"/sfdisk_sda.txt
    sfdisk -d /dev/sdb > "${v_temp_path}"/sfdisk_sdb.txt
    cp -a /etc/fstab "${v_temp_path}"/fstab.txt
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # Grub
  f_print_msg "\n" "Backing up Grub" "\n"
  v_temp_path="${v_workdir}"/grub
  if [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"/grub
    cp -a /boot/grub "${v_temp_path}"/boot_grub
    cp -a /etc/default/grub "${v_temp_path}"/default_grub.txt
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # Installed packages
  f_print_msg "\n" "Backing up installed software" "\n"
  v_temp_path="${v_workdir}"/software
  if [[ ! -d "${v_temp_path}" ]]; then
    mkdir -p "${v_temp_path}"
    dpkg -l | grep '^ii' > "${v_temp_path}"/dpkg_installed.txt
    apt-mark showmanual > "${v_temp_path}"/apt_manual.txt
    apt-mark showauto > "${v_temp_path}"/apt_auto.txt
    if [[ "$(command -v flatpak)" ]]; then
      flatpak list --app > "${v_temp_path}"/flatpak_installed.txt
    fi
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # CRON
  f_print_msg "\n" "Backing up root crons" "\n"
  v_temp_path="${v_workdir}"/crontab.txt
  if [[ ! -f "${v_temp_path}" ]]; then
    crontab -l > "${v_temp_path}"
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "${v_temp_path}"
  fi
  
  # MySQL/MariaDB
  f_print_msg "\n" "Backing up MySQL/MariaDB databases" "\n"
  v_temp_path="${v_workdir}"/mysql
  if [[ "$(command -v mysqldump)" ]] && [[ ! -d "${v_temp_path}" ]] && [[ -f "${v_mysql_cnf_path}" ]]; then
    mkdir -p "${v_temp_path}"
    if [[ "${v_mysql_single_dump}" == false ]]; then
      v_databases=$(mysql -e "SHOW DATABASES;" | grep -Ev 'Database|information_schema|performance_schema')
      # DO NOT double quote ${v_databases}. Word split does not work with double quoted vars.
      for v_db in ${v_databases}; do
        mysqldump --defaults-file="${v_mysql_cnf_path}" --force --opt --databases "${v_db}" > "${v_temp_path}/mysqldump_${v_db}.sql"
      done
    else
      mysqldump --defaults-file="${v_mysql_cnf_path}" --force --opt --all-databases > "${v_temp_path}/mysqldump_all.sql"
    fi
    f_backup_done_msg "${v_temp_path}"
  else
    f_backup_skipped_msg "No MySQL/MariaDB or missed ${v_mysql_cnf_path} or ${v_temp_path}"
  fi
  
  ### ENDING ###
  
  if [[ "${K_DEBUG}" != true ]]; then
    
    # Create tar archive
    f_print_msg "\n" "Creating TAR archive" "\n"
    v_filename="${v_backup_name}_${v_hostname}_${v_datetime_suffix}.tar.xz"
    tar --acls --xattrs -cpJf "${v_dest}/${v_filename}" --exclude="${v_dest}" "${v_workdir}" "${a_source_extra[@]}" >/dev/null 2>&1
    chmod 770 "${v_dest}/${v_filename}"
    f_print_msg "TAR archive ${v_filename} created in ${v_dest}" "\n"
    # Change tar archive ownership to current user
    if [[ "${v_tar_owner}" ]]; then
      f_print_msg "Changing tar archive ownership to user: $(users)" "\n"
      chown "$(users)":"$(users)" "${v_dest}/${v_filename}"
    fi
    f_print_msg "Done" "\n"
    
    # Remote rsync $dest directory
    f_print_msg "\n" "Synchronizing backup directory" "\n"
    if [[ "${v_dest_rsync}" ]]; then
      f_print_msg "rsync ${v_dest} with ${v_dest_rsync}" "\n"
      rsync -arq -AX --delete --force "${v_dest}" "${v_dest_rsync}"
      f_print_msg "Done" "\n"
    else
      f_print_msg "No rsync destination configured" "\n"
      f_print_msg "Skipped" "\n"
    fi
    
    # Remove $workdir directory
    if [[ "${v_quiet}" == false ]]; then
      printf "\nYou can now delete the temporary directory %s" "${v_workdir}"
      v_msg="Do you really want to delete it? ([Y]es/[N]o/[C]ancel)? "
      f_ask_confirmation "${v_msg}" "${v_workdir}"
    elif [[ "${v_quiet}" == true ]] && [[ "${v_keep_workdir}" == false ]]; then
      rm -rf "${v_workdir}"
    fi
    
  fi
  
  # Execute post command
  if [[ "${v_post_command}" ]]; then
    f_print_msg "\n" "Executing post command" "\n"
    eval "${v_post_command}"
  fi
  
  # All Done
  f_print_msg "\n" "All done" "\n"
  f_print_msg "" "\n"
  exit 0
}


########################
# MANAGE ARGUMENTS
########################


v_arguments=$(getopt -q -n "${0##*/}" \
  -o 'huvsqk' \
  -l 'help,usage,version,start,quiet,keep' \
  -- "${@}")
v_valid_arguments="${?}"
if [ "${v_valid_arguments}" != "0" ]; then
  f_help
  exit 1
fi
if [[ "${#}" -lt 1 ]]; then
  f_help
else
  v_start=false
  v_quiet=false
  v_keep_workdir=false
  eval set -- "${v_arguments}"
  while true; do
    case "${1}" in
      -h|--help)
        f_help
        break
        ;;
      -k|--keep)
        v_keep_workdir=true
        shift
        ;;
      -q|--quiet)
        v_quiet=true
        shift
        ;;
      -s|--start)
        v_start=true
        shift
        ;;
      -u|--usage)
        f_usage
        break
        ;;
      -v|--version)
        f_version
        break
        ;;
      --)
        shift
        break
        ;;
      *)
        f_help
        break
        ;;
    esac
  done
  if [[ "${v_start}" == true ]]; then
    main
  fi
fi

exit 0
