# DISCLAIMER

Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.

This code is licensed under GPLv3 license (see LICENSE for details)
The above copyright notice and this permission notice shall be
included and visible in the top of all copies or portions of this
Software.

This program is free software.
You can use, redistribute and/or modify it under the terms of the
GNU General Public License published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Author, Owner, Maintainer and Contributors are not responsable for
any damage or data loss. Use at your own risk.