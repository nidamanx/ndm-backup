# NDM Backup

Backup important parts of your GNU/Linux machine as:

- Hardware info
- Hardware devices list
- Block ID of installed devices (root is required)
- UEFI (root is required)
- BIOS MBR (root is required)
- LUKS configuration (root is required)
- LVM configuration (root is required)
- GNU/Linux release
- Linux Kernel release
- Grub configuration
- Partitions info (root is required)
- Installed packages
- Crontab (root is required)
- MySQL/MariaDB dump (root is required)
- More directories chosen by the user (root could be required)

All these informations will be compressed tar xz into a chosen directory.

Optionally, this software can also synchronize the backup directory with a local or remote directory using rsync.

Note: you can also execute your own customized commands before and after backing up your system.


### DOWNLOAD ###

You can clone/download the last version of the full project or just only the main script:
- clone with SSH: `git clone git@gitlab.com:nidamanx/ndm-backup.git`
- [clone full project with https](https://gitlab.com/nidamanx/ndm-backup.git)
- [download only the main script with https](https://gitlab.com/nidamanx/ndm-backup/-/raw/main/ndm_backup.sh?inline=false)


### INSTALL ###

1. Save this script (or an alias of it) in an appropriate directory (eg: `/usr/local/bin/` or `~/.local/bin/` or `~/bin/`)
2. Change script permission as follows: `chmod +x ndm-backup.sh`

Suggestion:
1. `sudo apt install -y git`
2. `mkdir ~/bin`
3. `cd ~/bin`
4. `git clone git@gitlab.com:nidamanx/ndm-backup.git`
5. `chmod +x ~/bin/ndm-backup/ndm_backup.sh`
6. `sudo ln -s ~/bin/ndm-backup/ndm_backup.sh /usr/local/bin/ndm_backup`

__MySQL/MariaDB backup__

1. `cp -a ~/bin/ndm-backup/my.cnf ~/bin/ndm-backup/.my.cnf`
2. `editor ~/bin/ndm-backup/.my.cnf` (set your mysql user/pass)
3. `chmod 600 ~/bin/ndm-backup/.my.cnf`

_Note: you can optionally save .my.cnf in a custom location_


### CONFIGURE ###

Configure the parameters in ndm_backup.sh appropriare section


### USAGE ###

This script needs root privileges to backup some parts of your GNU/Linux machine

`sudo ndm_backup` or `su -c 'ndm_backup'`


### HELP ###

This script needs root privileges to backup some parts of your GNU/Linux machine

`ndm_backup -h` or `ndm_backup --help'`


### LICENSE and DISCLAIMER ###

Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.

This code is licensed under GPLv3 license (see LICENSE for details)
The above copyright notice and this permission notice shall be
included and visible in the top of all copies or portions of this
Software.

This program is free software.
You can use, redistribute and/or modify it under the terms of the
GNU General Public License published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Author, Owner, Maintainer and Contributors are not responsable for
any damage or data loss. Use at your own risk.

---
---


#### NOTES ####

- This software is not intended to be a substitution of different backup solutions like backintime, timeshift, clonezilla and more. It's just an integration to store more parts of your GNU/Linux machine

- This software if fully tested with Debian 11 Bullseye

- Code check is done with `shellcheck` and `checkbashisms`
