Welcome to NDM Backup, and thank you for your interest in contributing!

Note that this software is certified for Debian. We cannot guarantee it will work without issues in all derivates.

# Opening an issue:
We welcome issues, whether to ask a question, provide information, request a new feature or to report a suspected bug or problem related to security and more.

When submitting a bug report, please provide the following information so that
we can handle the report more easily:
 - Software version. If you're not sure, open a terminal and type `ndm_backup --version`
 - Linux distribution. If you're not sure, open a terminal and type `lsb_release --all`
 - If you know that the problem you are submitting did not exist in an earlier version of this software, please mention it

# Opening an pull request:
Pull requests with enhancements or bugfixes are very welcome.
